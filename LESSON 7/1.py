# Задача 1

while True:
    name = input('Введи имя: ')
    if name.isalpha() and name.istitle():
        surname = input('Введи фамилию: ')
        if surname.isalpha() and surname.istitle():
            age = input('Введи возраст: ')
            if age.isdigit() and 18 <= int(age) <= 90:
                print('Все верно')
                break
            else:
                print('Неверные данные')
        else:
            print('Неверно')
    else:
        print('Неверные данные')
